package com.sun;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.sun.Mapper")
@SpringBootApplication
public class MyBatisApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(MyBatisApplication.class, args);
	}

	@Override
	public void run(String... args) {
		System.out.println("服务启动完成！");
	}

}
