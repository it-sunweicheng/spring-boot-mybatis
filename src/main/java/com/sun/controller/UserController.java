package com.sun.controller;

import com.sun.aspect.WebLog;
import com.sun.common.Result;
import com.sun.entity.User;
import com.sun.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/user")
@Api(value = "用户接口", tags = { "用户接口" })
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserController {

    private final UserService userService;

    @WebLog("新增用户")
    @ApiOperation("新增用户")
    @PostMapping("addUser")
    public Result<Void> addUser(@RequestBody User user) {
        userService.insertUser(user);
        return Result.success();
    }

    @WebLog("删除用户")
    @ApiOperation("删除用户")
    @GetMapping("deleteUser")
    public Result<Void> deleteUser(@RequestParam("id") Long id) {
        userService.deleteUser(id);
        return Result.success();
    }

    @WebLog("修改用户")
    @ApiOperation("修改用户")
    @PostMapping("modifyUser")
    public Result<Void> modifyUser(@RequestBody User user) {
        userService.updateUser(user);
        return Result.success();
    }

    @WebLog("获取用户")
    @ApiOperation("获取用户")
    @GetMapping("getUser")
    public Result<User> getUser(@RequestParam("id") Long id) {
        return new Result<>(userService.getUserById(id));
    }

    @ApiOperation("获取所有用户")
    @GetMapping("getAllUser")
    public Result<List<User>> getAllUser() {
        return new Result<>(userService.getAllUser());
    }

}
