package com.sun;

import com.sun.entity.User;
import com.sun.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class DemoApplicationTests {

    @Resource
    private UserMapper userMapper;

    @Test
    public void testInsert(){
        //数据生成
        List<User> userList = createData(100);

        //循环插入
        long start = System.currentTimeMillis();
        userList.stream().forEach(user -> userMapper.insert(user));
        System.out.println(System.currentTimeMillis() - start);
    }

    private List<User> createData(int size) {
        List<User> userList = new ArrayList<>();
        for(int i = 0; i < size; i++){
            userList.add(User.builder().build());
        }
        return userList;
    }

}
